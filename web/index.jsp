<%-- 
    Document   : index
    Created on : 18/04/2021, 6:06:42 p. m.
    Author     : BELSOFT
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="co.usuario.tuprograma.dao.ProductoDAO"%>
<%@page import="co.usuario.tuprograma.dao.ComponenteFlexibleDAO"%>
<%@page import="co.usuario.tuprograma.dao.PersonaNaturalDAO"%>
<%@page import="co.usuario.tuprograma.dao.CiudadDAO"%>
<%@page import="co.usuario.tuprograma.entity.PersonaNatural"%>
<%@page import="co.usuario.tuprograma.entity.ComponenteFlexible"%>
<%@page import="co.usuario.tuprograma.entity.Ciudad"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!--<meta http-equiv="refresh" content="1" >-->
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            ComponenteFlexibleDAO componenteFlexibleDAO=new ComponenteFlexibleDAO();
            for(ComponenteFlexible componenteFlexible:componenteFlexibleDAO.getComponentesFlexibles()){
                out.print("<br>"+componenteFlexible);
                
            }
                out.print("<br>---");
            
            PersonaNaturalDAO personaNaturalDAO=new PersonaNaturalDAO();
            for(PersonaNatural personaNatural: personaNaturalDAO.listar()){
                out.print("<br>"+personaNatural);
            }
            
                out.print("<br>---");
            
            CiudadDAO ciudadDAO=new CiudadDAO();
            for(Ciudad ciudad :ciudadDAO.listar()){
                out.print("<br>"+ciudad);
            }

        %>
    </body>
</html>
