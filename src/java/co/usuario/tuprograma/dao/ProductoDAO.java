package co.usuario.tuprograma.dao;

import co.usuario.tuprograma.bean.Producto;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
public class ProductoDAO extends ConnectionMariadbDAO {

    public List<Producto> listarPorUnidad(String nombreUnidad) {
        List<Producto> productos = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection con = conectarBD()) {
            ps = con.prepareStatement("select producto.* from producto\n"
                    + "inner join producto_unidad\n"
                    + "on producto.prod_id=producto_unidad.prod_id\n"
                    + "inner join unidad \n"
                    + "on unidad.unidad_id=producto_unidad.unidad_id \n"
                    + "and unidad.unidad_nombre=?");
            ps.setString(1, nombreUnidad);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setId(rs.getInt("prod_id"));
                producto.setNombre(rs.getString("prod_nombre"));
                productos.add(producto);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ProductoDAO.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            productos = null;
        } finally {
            cerrar(rs, ps);
        }
        return productos;
    }

    public List<Producto> listarPorUnidad_(String nombreUnidad) {
        List<Producto> productos = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection con = conectarBD()) {
            ps = con.prepareStatement(getSQL("LISTAR_POR_UNIDAD"));
            ps.setString(1, nombreUnidad);
            rs = ps.executeQuery();
            while (rs.next()) {
                Producto producto = new Producto();
                producto.setId(rs.getInt("prod_id"));
                producto.setNombre(rs.getString("prod_nombre"));
                productos.add(producto);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            productos = null;
        } finally {
            cerrar(rs, ps);
        }
        return productos;
    }
}
