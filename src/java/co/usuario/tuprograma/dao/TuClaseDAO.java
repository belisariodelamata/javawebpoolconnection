package co.usuario.tuprograma.dao;

import co.usuario.tuprograma.bean.Persona;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.List;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author BELSOFT
 */
public class TuClaseDAO extends ConnectionMariadbDAO {

    public List<Persona> listar() {
        List<Persona> personas = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection con = conectarBD()) {
            ps = con.prepareStatement(getSQL("OBTENER_PERSONAS"));
            rs = ps.executeQuery();
            while (rs.next()) {
                Persona persona = new Persona();
                persona.setPegeNombre(rs.getString("pege_nombres"));
                persona.setPegeApellidos(rs.getString("pege_apellidos"));
                personas.add(persona);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            personas = null;
        } finally {
            cerrar(rs, ps);
        }
        return personas;
    }
}
