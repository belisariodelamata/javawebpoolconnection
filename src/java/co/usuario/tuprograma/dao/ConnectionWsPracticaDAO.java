/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.dao;

import co.bs.dao.BSParentDAO;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author BELSOFT
 */
public class ConnectionWsPracticaDAO extends BSParentDAO{

    protected Connection conectarBD() throws SQLException {
        return super.conectarBD("ws_practica");
    }
    
}
