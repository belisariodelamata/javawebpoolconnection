/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.dao;

import co.usuario.tuprograma.entity.PersonaNatural;
import co.usuario.tuprograma.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class PersonaNaturalDAO extends ConnectionWsPracticaDAO {

    public List<PersonaNatural> listar() {
        List<PersonaNatural> lista = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection con = conectarBD()) {
            ps = con.prepareStatement("select * from personanatural");
            rs = ps.executeQuery();
            Util.esperar((int)(Math.random()*2000));
            while (rs.next()) {
                PersonaNatural registro = new PersonaNatural();
                registro.setPege_id(rs.getInt("pege_id"));
                registro.setPege_nombres(rs.getString("pege_nombres"));
                registro.setPege_apellidos(rs.getString("pege_apellidos"));
                lista.add(registro);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            lista = null;
        } finally {
            cerrar(rs, ps);
        }
        return lista;
    }
}
