package co.usuario.tuprograma.dao;

import co.bs.dao.BSParentDAO;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author BELSOFT
 */
public class ConnectionPostgresqlDAO extends BSParentDAO{

    protected Connection conectarBD() throws SQLException {
        return super.conectarBD("basePostgresql");
    }
    
}
