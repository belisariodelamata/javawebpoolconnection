package co.usuario.tuprograma.dao;

import co.usuario.tuprograma.bean.Persona;
import co.usuario.tuprograma.entity.Ciudad;
import co.usuario.tuprograma.entity.PersonaNatural;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author BELSOFT
 */
public class CiudadDAO extends ConnectionWsPracticaDAO {

    public List<Ciudad> listar() {
        List<Ciudad> lista = new LinkedList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try (Connection con = conectarBD()) {
            ps = con.prepareStatement("select * from ciudad");
            rs = ps.executeQuery();
            while (rs.next()) {
                Ciudad registro = new Ciudad();
                registro.setCiudadNombre(rs.getString("ciudad_nombre"));
                lista.add(registro);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            lista = null;
        } finally {
            cerrar(rs, ps);
        }
        return lista;
    }
}
