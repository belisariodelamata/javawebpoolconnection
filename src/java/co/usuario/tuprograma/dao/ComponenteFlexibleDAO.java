/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.dao;

import co.usuario.tuprograma.entity.ComponenteFlexible;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
public class ComponenteFlexibleDAO extends ConnectionPostgresqlDAO{
    
    public List<ComponenteFlexible> getComponentesFlexibles() {
        List<ComponenteFlexible> lcf = new LinkedList<>();
        Connection con = null;
        ResultSet rs = null;
        try {
            con = conectarBD();
            PreparedStatement ps = con.prepareStatement(getSQL("OBTENER_COMPONENTES_FLEXIBLES"));
            rs = ps.executeQuery();
            while (rs.next()) {
                ComponenteFlexible cf = new ComponenteFlexible();
                cf.setId(rs.getInt("id_componente"));
                cf.setCodigo(rs.getString("nombre"));
                cf.setDescripcion(rs.getString("nombre") + "(" + rs.getString("nombre_area") + ")");
                lcf.add(cf);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ComponenteFlexibleDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            cerrar(con);
            cerrar(rs);
        }
        return lcf;
    }    
}
