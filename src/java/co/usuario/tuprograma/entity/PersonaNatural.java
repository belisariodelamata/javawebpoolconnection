/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.entity;

/**
 *
 * @author BELSOFT
 */
public class PersonaNatural {
    private int pege_id;
    private String pege_nombres;
    private String pege_apellidos;

    public int getPege_id() {
        return pege_id;
    }

    public void setPege_id(int pege_id) {
        this.pege_id = pege_id;
    }

    public String getPege_nombres() {
        return pege_nombres;
    }

    public void setPege_nombres(String pege_nombres) {
        this.pege_nombres = pege_nombres;
    }

    public String getPege_apellidos() {
        return pege_apellidos;
    }

    public void setPege_apellidos(String pege_apellidos) {
        this.pege_apellidos = pege_apellidos;
    }

    @Override
    public String toString() {
        return "["+this.getPege_id()+":"+this.getPege_nombres()+":"+this.getPege_apellidos()+"]";
    }
    
    
}
