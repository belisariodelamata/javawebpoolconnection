package co.usuario.tuprograma.entity;

/**
 *
 * @author BELSOFT
 */
public class Ciudad {
    private String ciudadIdDian;
    private String ciudadNombre;
    private String depIdDian;
    private Integer ciudadId;

    public String getCiudadIdDian() {
        return ciudadIdDian;
    }

    public void setCiudadIdDian(String ciudadIdDian) {
        this.ciudadIdDian = ciudadIdDian;
    }

    public String getCiudadNombre() {
        return ciudadNombre;
    }

    public void setCiudadNombre(String ciudadNombre) {
        this.ciudadNombre = ciudadNombre;
    }

    public String getDepIdDian() {
        return depIdDian;
    }

    public void setDepIdDian(String depIdDian) {
        this.depIdDian = depIdDian;
    }

    public Integer getCiudadId() {
        return ciudadId;
    }

    public void setCiudadId(Integer ciudadId) {
        this.ciudadId = ciudadId;
    }

    @Override
    public String toString() {
        return "["+this.ciudadNombre+"]";
    }
    
    
    
}
