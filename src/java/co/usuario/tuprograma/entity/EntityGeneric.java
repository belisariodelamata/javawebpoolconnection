package co.usuario.tuprograma.entity;

public class EntityGeneric<E> {

    private E id;
    private String codigo;
    private String descripcion;

    public E getId() {
        return id;
    }

    public void setId(E id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof EntityGeneric) {
            return this.id.equals(((EntityGeneric) obj).id);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "[" + id + ":" + this.getCodigo() + "]";
    }

}
