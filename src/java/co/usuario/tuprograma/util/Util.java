package co.usuario.tuprograma.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author BELSOFT
 */
public class Util {
    
    public static void esperar(long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
