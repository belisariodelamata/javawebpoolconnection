/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.usuario.tuprograma.bean;

/**
 *
 * @author BELSOFT
 */
public class Persona {
    private String pege_nombre;
    private String pege_apellidos;

    public String getPege_nombre() {
        return pege_nombre;
    }

    public void setPegeNombre(String pege_nombre) {
        this.pege_nombre = pege_nombre;
    }

    public String getPege_apellidos() {
        return pege_apellidos;
    }

    public void setPegeApellidos(String pege_apellidos) {
        this.pege_apellidos = pege_apellidos;
    }
    
}
